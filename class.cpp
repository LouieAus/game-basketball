#include <SFML/Graphics.hpp>
#include <iostream>
#include "class.h"

namespace shape {
	Ball::Ball(float r, float x, float y, color c) {
		BallInit(r, x, y, c, ball);
	}
	void Ball::BallInit(float r, float x, float y, color c, sf::CircleShape &ball) {
		radius = r;
		cX = x;
		cY = y;
		Color.r = c.r;
		Color.g = c.g;
		Color.b = c.b;
		ball = Create();
	}
	sf::CircleShape Ball::Create(){
		sf::CircleShape shapeBall(radius);
		shapeBall.setFillColor(sf::Color(Color.r, Color.b, Color.b));
		shapeBall.setOrigin(radius, radius);
		shapeBall.setPosition(cX, cY);
		return shapeBall;
	}


	Basket::Basket(float x, float y, float w, float h, color c) {
		BasketInit(x, y, w, h, c, basket);
	}
	void Basket::BasketInit(float x, float y, float w, float h, color c, sf::RectangleShape &basket) {
		cX = x;
		cY = y;
		wide = w;
		high = h;
		Color = c;
		basket = Create();
	}
	sf::RectangleShape Basket::Create() {
		sf::RectangleShape basket(sf::Vector2f(wide, high));
		basket.setFillColor(sf::Color(Color.r, Color.b, Color.b));
		basket.setPosition(cX, cY);
		return basket;
	}
}