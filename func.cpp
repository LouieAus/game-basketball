#include <math.h>
#include "func.h"
namespace operation {
	float MouseAngle(float x, float y, float centerX, float centerY) {
		float X = Abs(x - centerX);
		float Y = Abs(y - centerY);
		float atg = atan(Y / X);
		return atg;
	}
	float MouseDistance(float x, float y, float centerX, float centerY) {
		float X = Abs(x - centerX);
		float Y = Abs(y - centerY);
		return sqrt(X * X + Y * Y);
	}
	float Abs(float num) {
		if (num < 0) return -1 * num;
		else return num;
	}
	bool IncCheck(float beginA, float endA, float beginB, float endB) {
		if (((beginB >= beginA) && (beginB <= endA)) ||
			((endB >= beginA) && (endB <= endA))) {
			return 1;
		}
		else
			return 0;
	}
}