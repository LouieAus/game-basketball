#pragma once
#include <SFML/Graphics.hpp>
namespace shape {
	struct color {
		int r, g, b;
	};
	//����� ����
	class Ball {
	private:
		color Color{0, 0, 0};
	public:
		float radius, cX, cY, nullSpeed, angle, rotationAngle;;
		sf::CircleShape ball;
		Ball(float r, float x, float y, color c);
		void BallInit(float r, float x, float y, color c, sf::CircleShape& ball);
		sf::CircleShape Create();
	};

	//����� �������
	class Basket {
	private:
		color Color{ 0, 0, 0 };
	public:
		float cX, cY, wide, high;
		sf::RectangleShape basket;
		Basket(float x, float y, float w, float h, color c);
		void BasketInit(float x, float y, float w, float h, color c, sf::RectangleShape& basket);
		sf::RectangleShape Create();
	};
}
