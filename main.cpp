﻿#include <iostream>
#include <thread>
#include <chrono>
#include <SFML/Graphics.hpp>
#include <math.h>
#include "class.h"
#include "func.h"

#define pi 3.14
#define G -9.8

using namespace std::chrono_literals;

int main() {
	std::chrono::milliseconds time = 10ms;

	//Окно
	int winX = 800;
	int winY = 600;
	sf::RenderWindow window(sf::VideoMode(winX, winY), "Basketball");
	sf::Texture back;
	if (!back.loadFromFile("resource/back_1.jpg"))
		std::cout << "ERROR: no loaded texture!" << '\n';
	sf::Sprite sprite(back);

	//Мяч
	shape::color Color{ 30, 20, 200 };
	float ballX = 70;
	float ballY = 450;
	float ballRadius = 30;
	shape::Ball ball_1(ballRadius, ballX, ballY, Color);
	sf::Texture ballTexture;
	if (!ballTexture.loadFromFile("resource/ball.png"))
		std::cout << "ERROR: no loaded texture!" << '\n';
	ball_1.ball.setTexture(&ballTexture);

	//Корзина
	float baskX = 650;
	float baskY = 250;
	float ring = 130;
	shape::color basketColor{ 100, 100, 100 };
	shape::Basket basket_1(baskX, baskY, 2, 15, basketColor); //край
	shape::Basket basket_2(baskX + ring, baskY, 2, 15, basketColor); //край
	shape::Basket basketRing(baskX, baskY, ring + 2, 15, basketColor); //кольцо
	shape::Basket basketMesh(baskX+10, baskY + 15, ring + 2 -15, ring + 2-30, basketColor); //сетка
	sf::Texture meshTexture;
	if (!meshTexture.loadFromFile("resource/mesh.png"))
		std::cout << "ERROR: no loaded texture!" << '\n';
	basketMesh.basket.setTexture(&meshTexture);
	shape::Basket basketBoard(baskX + ring + 2, baskY - 80, 20, 160, {61, 43, 31}); //доска


	//Триггеры
	shape::Basket trigger_1(baskX + 55, baskY + 15, 20, 2, basketColor);
	shape::Basket trigger_2(baskX + 55, baskY + 80, 30, 1, basketColor);

	//Текст счёта
	sf::Text scoreText;
	sf::Font font;
	if (!font.loadFromFile("resource/arcclassic.TTF"))
		std::cout << "ERROR: no loaded font!" << '\n';
	scoreText.setFont(font);
	scoreText.setCharacterSize(35);
	scoreText.setPosition(10, 10);
	scoreText.setFillColor(sf::Color::Black);

	//Текст победы/поражения
	sf::Text resultText;
	resultText.setFont(font);
	resultText.setCharacterSize(40);
	resultText.setPosition(370, 170);
	sf::Color orange(255, 165, 0);
	resultText.setFillColor(sf::Color::Transparent);

	float mouseX, mouseY;
	float nullCoord[2] = { ball_1.cX, ball_1.cY };
	float kX, tick, tickTime, throwTime;
	bool init = 1;
	bool ballThrow = 0;
	bool trigger = 0;
	int score = 0;
	int resultCount = 0;
	std::string result;

	while (window.isOpen()) {
		sf::Event event;
		if (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window.close();
		}
		//При "прицеливании"
		if (init) {
			sf::Vector2i localPosition = sf::Mouse::getPosition(window);
			mouseX = localPosition.x;
			mouseY = localPosition.y;

			float angle = ((-1) * operation::MouseAngle(mouseX, mouseY, ballX, ballY)) * 180 / pi;
			if ((mouseX >= ballX) && (mouseY <= ballY))
				ball_1.ball.setRotation(angle);
		}
		//При броске
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (init)) {
			ball_1.angle = (operation::MouseAngle(mouseX, mouseY, ballX, ballY));
			ball_1.nullSpeed = (operation::MouseDistance(mouseX, mouseY, ballX, ballY)) / 5;
	
			nullCoord[0] = ballX;
			nullCoord[1] = ballY;
			kX = 1;
			tick = 0;
			tickTime = 0.1;
			throwTime = 0;
			
			trigger = 0;
			init = 0;
			ballThrow = 1;
			ball_1.rotationAngle = ball_1.angle * 180 / pi;
		}
		//При процессе броска
		if (ballThrow) {
			//Физика мяча
			throwTime = throwTime + tickTime;
			tick = tick + tickTime;
			float nullSpeed_x = ball_1.nullSpeed * cos(ball_1.angle); //скорость по Ox
			float nullSpeed_y = ball_1.nullSpeed * sin(ball_1.angle); //скорость по Oy
			float currentSpeed = ball_1.nullSpeed - G * tick; //текущая скорость
			
			if (((ball_1.cX + ballRadius) >= winX) || ((ball_1.cX - ballRadius) <= -1)) {
				kX *= -1;
			}
			if ((ball_1.cY + ballRadius) > (winY + 1)) {
				ball_1.nullSpeed = currentSpeed / 3.5; //3.5
				nullCoord[0] = ball_1.cX;
				nullCoord[1] = winY - ballRadius;
				tick = 0;
			}
			//Проверка на столкновение с краями корзины
			if (((operation::IncCheck(ball_1.cX - ballRadius, ball_1.cX + ballRadius, baskX-1, baskX+1 + 2))
				&& (operation::IncCheck(ball_1.cY - ballRadius, ball_1.cY + ballRadius, baskY, baskY + 15))) ||
				((operation::IncCheck(ball_1.cX - ballRadius, ball_1.cX + ballRadius, baskX+ring - 1, baskX+ring + 1 + 2))
					&& (operation::IncCheck(ball_1.cY - ballRadius, ball_1.cY + ballRadius, baskY, baskY + 15)))){
				kX *= -1;
			}

			ball_1.rotationAngle = ball_1.rotationAngle + kX*6 * (nullSpeed_x / 100);
			ball_1.cX = ball_1.cX + kX * nullSpeed_x * tickTime;
			ball_1.cY = nullCoord[1] - (nullSpeed_y * tick + (G * tick * tick) / 2);

			if (ball_1.nullSpeed < 10) {
				ball_1.cY = winY - ballRadius;
				if (((int)tick % 1 == 0) && (kX < -0.1 || kX > 0.1))
					kX -= kX * 0.01;
				else if ((kX > -0.1) && (kX < 0.1))
					kX = 0;
			}
			//Проверка на задевания триггеров
			if ((operation::IncCheck(ball_1.cX - ballRadius, ball_1.cX + ballRadius, trigger_1.cX, trigger_1.cX + 20)) &&
				(operation::IncCheck(ball_1.cY - ballRadius, ball_1.cY + ballRadius, trigger_1.cY, trigger_1.cY + 2))) {
				trigger = 1;
			}
			if ((operation::IncCheck(ball_1.cX - ballRadius, ball_1.cX + ballRadius, trigger_2.cX, trigger_2.cX + 30)) &&
				(operation::IncCheck(ball_1.cY - ballRadius, ball_1.cY + ballRadius, trigger_2.cY, trigger_2.cY + 2))) {
				if (trigger) {
					score++;
					ballThrow = 0;
					init = 1;
					ballX = rand() % 430 + 20;
					ball_1.cX = ballX;
					ball_1.cY = ballY;
					ball_1.rotationAngle = 0;
					result = "WIN";
					resultText.setFillColor(orange);
					resultCount = 0;
				}
				else
					throwTime = 36;
			}
			//Проверка на время
			if (throwTime > 35) {
				ballThrow = 0;
				init = 1;
				ballX = rand() % 430 + 20;
				ball_1.cX = ballX;
				ball_1.cY = ballY;
				ball_1.rotationAngle = 0;
				result = "FAIL";
				resultText.setFillColor(sf::Color::Red);
				resultCount = 0;
			}
			//Обновление значений
			ball_1.ball.setPosition(ball_1.cX, ball_1.cY);
			ball_1.ball.setRotation(ball_1.rotationAngle);
		}

		if (resultCount<=150)
			resultCount++;
		else
			result = "";

		resultText.setString(result);
		scoreText.setString("Score  " + std::to_string(score));

		//Отрисовка
		window.clear(sf::Color::Black);
		window.draw(trigger_1.basket);
		window.draw(trigger_2.basket);
		window.draw(sprite);
		window.draw(scoreText);
		window.draw(resultText);
		window.draw(ball_1.ball);
		window.draw(basket_1.basket);
		window.draw(basket_2.basket);
		window.draw(basketRing.basket);
		window.draw(basketBoard.basket);
		window.draw(basketMesh.basket);
		window.display();
		std::this_thread::sleep_for(time);
	}
	return 0;
}